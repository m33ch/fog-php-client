<?php

use Advision\Fog\Authenticator;
use Advision\Fog\Client;
use Advision\Fog\Models\Recipient;

class RecipientApiTest {

    public static function create($client)
    {
        $recipient = new \Advision\Fog\Models\Recipient();

        $recipient->setName('Recipient '.rand(1,99))
            ->setFields([
                    [
                        'type' => "input",
                        'inputType' => "text",
                        'label' => "ID (disabled text field)",
                        'model' => "id",
                        'readonly' => true,
                        'disabled' => true
                    ],
                    [
                        'type' => "input",
                        'inputType' => "text",
                        'label' => "Name",
                        'model' => "name",
                        'placeholder' => "Your name",
                        'featured' => true,
                        'required' => true
                    ]
            ]);

        return $client->save($recipient);
    }

    public static function update($client, $id, $data)
    {
        $recipient = \Advision\Fog\Factories\Recipient::fromArray($data);

        $recipient->setId($id);

        return $client->save($recipient);
    }

    public static function find($client, $id)
    {
        $recipient = \Advision\Fog\Factories\Recipient::fromArray([]);

        $recipient->setId($id);

        return $client->find($recipient);
    }

    public static function delete($client, $id)
    {
        $recipient = \Advision\Fog\Factories\Recipient::fromArray([]);

        $recipient->setId($id);

        return $client->delete($recipient);
    }

}
///CREATE
/*try {

    $auth = new Authenticator(1223, $api_key, $api_password);

    $token = $auth->getToken();

    $client = new Client($token);

    $recipient = RecipientApiTest::create($client);

    dd($recipient);

} catch (Exception $e) {
    dd($e->getMessage(), $e->getCode());
}*/
///UPDATE
/*try {

    $auth = new Authenticator(1223, $api_key, $api_password);

    $token = $auth->getToken();

    $client = new Client($token);

    $recipient = RecipientApiTest::update($client, '597703cb18388a49d56f3390', [
        'name' => 'Recipient modificata',
        'fields_list' => [
                    [
                        'type' => "input",
                        'inputType' => "text",
                        'label' => "ID MODIFICATO",
                        'model' => "id",
                        'readonly' => true,
                        'disabled' => true
                    ],
                ]
    ]);

    dd($recipient);

} catch (Exception $e) {
    dd($e->getMessage(), $e->getCode());
}*/
//597703cb18388a49d56f3390
//FIND ONE
/*try {

    $auth = new Authenticator(1223, $api_key, $api_password);

    $token = $auth->getToken();

    $client = new Client($token);

    $recipient = RecipientApiTest::find($client, '597703cb18388a49d56f3390');

    dd($recipient);

} catch (Exception $e) {
    dd($e->getMessage(), $e->getCode());
}*/
///// DELETE
try {

    $auth = new Authenticator(1223, $api_key, $api_password);

    $token = $auth->getToken();

    $client = new Client($token);

    $recipient = RecipientApiTest::delete($client, "597703cb18388a49d56f3390");

    dd($recipient);

} catch (Exception $e) {
    dd($e->getMessage(), $e->getCode());
}

/*$r = createList($host, [
    'auth_token' => $token,
    'name' => 'lista_test',
    'fields_list' => [
        [
            'type' => "input",
            'inputType' => "text",
            'label' => "ID (disabled text field)",
            'model' => "id",
            'readonly' => true,
            'disabled' => true
        ],
        [
            'type' => "input",
            'inputType' => "text",
            'label' => "Name",
            'model' => "name",
            'placeholder' => "Your name",
            'featured' => true,
            'required' => true
        ],
        [
            'type' => "input",
            'inputType' => "password",
            'label' => "Password",
            'model' => "password",
            'min' => 6,
            'required' => true,
            'hint' => "Minimum 6 characters",
            'validator' => VueFormGenerator.validators.string
        ],
        [
            'type' => "select",
            'label' => "Skills",
            'model' => "skills",
            'values' => ["Javascript", "VueJS", "CSS3", "HTML5"]
        ],
        [
            'type' => "input",
            'inputType' => "email",
            'label' => "E-mail",
            'model' => "email",
            'placeholder' => "User's e-mail address"
        ],
        [
            'type' => "checkbox",
            'label' => "Status",
            'model' => "status",
            'default' => true
        ]
    ]
]);
dd($r);*/

/*$r = updateList($host, "59380ce5d9cdcf7010a565fd", [
    'auth_token' => $token,
    'name' => 'lista_test_2'
]);
dd($r);*/

// $r = getList($host, $token, "59380ce5d9cdcf7010a565fd");
// dd($r);

// $r = deleteList($host, $token, "59380c0ade42996e89206bed");
// dd($r);

// $r = getLists($host, $token);
// dd($r);
