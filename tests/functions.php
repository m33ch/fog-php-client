<?php

    //require_once(__DIR__ . '/vendor/autoload.php');

    function dd($value) {
        echo '<pre>';
        print_r($value);
        echo '</pre>';
        die();
    }

    function getGuzzleHttp($host) {
        return new GuzzleHttp\Client([
            'base_uri' => $host,
            'http_errors' => true
        ]);
    }

    function getToken($host, $api_key, $api_password, $integration_user_id) {
      $client = getGuzzleHttp($host);

      $res = $client->request('post', '/v1/core/application/getToken', [
          'form_params' => [
              'api_key' => $api_key,
              'api_password' => $api_password,
              'integration_user_id' => $integration_user_id
          ]
      ]);

      $r = json_decode($res->getBody());

      return isset($r->token) ? $r->token : null;
    }

    function getApp($host, $token) {
        $client = getGuzzleHttp($host);

        $res = $client->request('post', '/v1/core/application/getApplication', [
            'form_params' => [
                'auth_token' => $token
            ]
        ]);

        $r = json_decode($res->getBody());

        return isset($r->url) ? $r->url : null;
    }

    //--------------------------------------------------------------------------
    // GROUPS
    function listGroups($host, $token) {
        $client = getGuzzleHttp($host);

        try {
            $res = $client->request('get', '/v1/group', [
                'form_params' => [
                    'auth_token' => $token
                ]
            ]);

            $r = json_decode($res->getBody());

            return $r;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    function createGroup($host, $params) {
        $client = getGuzzleHttp($host);

        try {
            $res = $client->request('post', '/v1/group', [
                'form_params' => $params
            ]);

            $r = json_decode($res->getBody());

            return $r;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    function updateGroup($host, $id, $params) {
        $client = getGuzzleHttp($host);

        try {
            $res = $client->request('put', '/v1/group/' . $id, [
                'form_params' => $params
            ]);

            $r = json_decode($res->getBody());

            return $r;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    function getGroup($host, $token, $id) {
        $client = getGuzzleHttp($host);

        try {
            $res = $client->request('get', '/v1/group/' . $id, [
                'form_params' => [
                    'auth_token' => $token
                ]
            ]);

            $r = json_decode($res->getBody());

            return $r;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    function deleteGroup($host, $token, $id) {
        $client = getGuzzleHttp($host);

        try {
            $res = $client->request('delete', '/v1/group/' . $id, [
                'form_params' => [
                    'auth_token' => $token
                ]
            ]);

            $r = json_decode($res->getBody());

            return $r;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    //--------------------------------------------------------------------------
    // LISTS
    function getLists($host, $token) {
        $client = getGuzzleHttp($host);

        try {
            $res = $client->request('get', '/v1/list', [
                'form_params' => [
                    'auth_token' => $token
                ]
            ]);

            $r = json_decode($res->getBody());

            return $r;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    function createList($host, $params) {
        $client = getGuzzleHttp($host);

        try {
            $res = $client->request('post', '/v1/list', [
                'form_params' => $params
            ]);

            $r = json_decode($res->getBody());

            return $r;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    function updateList($host, $id, $params) {
        $client = getGuzzleHttp($host);

        try {
            $res = $client->request('put', '/v1/list/' . $id, [
                'form_params' => $params
            ]);

            $r = json_decode($res->getBody());

            return $r;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    function getList($host, $token, $id) {
        $client = getGuzzleHttp($host);

        try {
            $res = $client->request('get', '/v1/list/' . $id, [
                'form_params' => [
                    'auth_token' => $token
                ]
            ]);

            $r = json_decode($res->getBody());

            return $r;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    function deleteList($host, $token, $id) {
        $client = getGuzzleHttp($host);

        try {
            $res = $client->request('delete', '/v1/list/' . $id, [
                'form_params' => [
                    'auth_token' => $token
                ]
            ]);

            $r = json_decode($res->getBody());

            return $r;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    //--------------------------------------------------------------------------
    // FORMS
    function listForms($host, $token, $groupId) {
        $client = getGuzzleHttp($host);

        try {
            $res = $client->request('get', '/v1/form/group/' . $groupId, [
                'form_params' => [
                    'auth_token' => $token
                ]
            ]);

            $r = json_decode($res->getBody());

            return $r;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    function getForm($host, $token, $id) {
        $client = getGuzzleHttp($host);

        try {
            $res = $client->request('get', '/v1/form/' . $id, [
                'form_params' => [
                    'auth_token' => $token
                ]
            ]);

            $r = json_decode($res->getBody());

            return $r;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    function deleteForm($host, $token, $id) {
        $client = getGuzzleHttp($host);

        try {
            $res = $client->request('delete', '/v1/form/' . $id, [
                'form_params' => [
                    'auth_token' => $token
                ]
            ]);

            $r = json_decode($res->getBody());

            return $r;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    function createForm($host, $params) {
        $client = getGuzzleHttp($host);

        file_put_contents(__DIR__ . "/diocane.txt", date("Y-m-d H:i:s") . " - passo\n", FILE_APPEND);

        try {
            $res = $client->request('post', '/v1/form', [
                'form_params' => $params
            ]);

            $r = json_decode($res->getBody());

            return $r;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    function updateForm($host, $id, $params) {
        $client = getGuzzleHttp($host);

        try {
            $res = $client->request('put', '/v1/form/' . $id, [
                'form_params' => $params
            ]);

            $r = json_decode($res->getBody());

            return $r;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    // - themes

    function getFormThemes($host, $token, $type) {
        $client = getGuzzleHttp($host);

        try {
            $res = $client->request('get', '/v1/theme/type/' . $type, [
                'form_params' => [
                    'auth_token' => $token
                ]
            ]);

            $r = json_decode($res->getBody());

            return $r;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    function setFormTheme($host, $token, $formId, $themeSetup) {
        $client = getGuzzleHttp($host);

        try {
            $res = $client->request('post', '/v1/form/' . $formId . '/setTheme', [
                'form_params' => [
                    'auth_token' => $token,
                    'theme_setup' => $themeSetup
                ]
            ]);

            $r = json_decode($res->getBody());

            return $r;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    // - lists

    function setFormList($host, $token, $formId, $listId) {
        $client = getGuzzleHttp($host);

        try {
            $res = $client->request('post', '/v1/form/' . $formId . '/setList', [
                'form_params' => [
                    'auth_token' => $token,
                    'list_id' => $listId
                ]
            ]);

            $r = json_decode($res->getBody());

            return $r;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    // - fields

    function setFormFields($host, $token, $formId, $pageLabel, $fields) {
        $client = getGuzzleHttp($host);

        try {
            $res = $client->request('post', '/v1/form/' . $formId . '/setFields', [
                'form_params' => [
                    'auth_token' => $token,
                    'pageLabel' => $pageLabel,
                    'fields' => $fields
                ]
            ]);

            $r = json_decode($res->getBody());

            return $r;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    //--------------------------------------------------------------------------
    // CONFIG
    function getConfig($host, $token) {
        $client = getGuzzleHttp($host);

        try {
            $res = $client->request('get', '/v1/config', [
                'form_params' => [
                    'auth_token' => $token
                ]
            ]);

            $r = json_decode($res->getBody());

            return $r;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    function createConfig($host, $params) {
        $client = getGuzzleHttp($host);

        try {
            $res = $client->request('post', '/v1/config', [
                'form_params' => $params
            ]);

            $r = json_decode($res->getBody());

            return $r;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    function updateConfig($host, $id, $params) {
        $client = getGuzzleHttp($host);

        try {
            $res = $client->request('put', '/v1/config/' . $id, [
                'form_params' => $params
            ]);

            $r = json_decode($res->getBody());

            return $r;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    function deleteConfig($host, $token, $id) {
        $client = getGuzzleHttp($host);

        try {
            $res = $client->request('delete', '/v1/config/' . $id, [
                'form_params' => [
                    'auth_token' => $token
                ]
            ]);

            $r = json_decode($res->getBody());

            return $r;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
