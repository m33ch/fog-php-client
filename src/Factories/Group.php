<?php

namespace Advision\Fog\Factories;

use Advision\Fog\Models\Group as GroupModel;

class Group extends SingletonFactory
{
    protected static $properties = [
        '_id'                   => null,
        'integration_id'        => null,
        'integration_user_id'   => null,
        'total_forms'           => 0,
        'total_subscribers'     => 0,
        'name'                  => '',
        'sites_allowed'         => [],
        'forms'                 => []
    ];

    public static function fromObject($data)
    {
        $data = objectToArray($data);

        return self::fromArray($data);
    }

    public static function fromArray(array $data)
    {
        self::$properties = array_replace_recursive(self::$properties, $data);

        $model = new GroupModel;

        $model->setId(self::$properties['_id'])
              ->setName(self::$properties['name'])
              ->setSites(self::$properties['sites_allowed'])
              ->setIntegrationId(self::$properties['integration_id'])
              ->setTotalForms(self::$properties['total_forms'])
              ->setTotalSubscribers(self::$properties['total_subscribers'])
              ->setForms(self::$properties['forms'])
              ->setIntegrationUserId(self::$properties['integration_user_id']);

        return $model;
    }
}
