<?php

namespace Advision\Fog\Factories;

use Advision\Fog\Models\Recipient as RecipientModel;

class Recipient extends SingletonFactory
{
    protected static $properties = [
        '_id'                   => null,
        'integration_id'        => null,
        'integration_user_id'   => null,
        'name'                  => '',
        'fields_list'           => []
    ];

    public static function fromObject($data)
    {
        $data = objectToArray($data);

        return self::fromArray($data);
    }

    public static function fromArray(array $data)
    {
        self::$properties = array_replace_recursive(self::$properties, $data);

        $model = new RecipientModel;

        $model->setId(self::$properties['_id'])
              ->setName(self::$properties['name'])
              ->setFields(self::$properties['fields_list'])
              ->setIntegrationId(self::$properties['integration_id'])
              ->setIntegrationUserId(self::$properties['integration_user_id']);

        return $model;
    }
}
