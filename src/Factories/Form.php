<?php

namespace Advision\Fog\Factories;

use Advision\Fog\Models\Form as FormModel;

class Form extends SingletonFactory
{
    protected static $properties = [
        '_id'                   => null,
        'integration_id'        => null,
        'integration_user_id'   => null,
        'name'                  => '',
        'schedule'              => null,
        'formgroup_id'          => null,
        'formlist_id'           => null,
        'isActive'              => false,
        'actionUrl'             => null,
        'theme'                 => [
              'type' => 'embedded',
              'name' => 'fog_base'
        ]
    ];

    public static function fromObject($data)
    {
        $data = objectToArray($data);

        return self::fromArray($data);
    }

    public static function fromArray(array $data)
    {
        self::$properties = array_replace_recursive(self::$properties, $data);

        $model = new FormModel;

        $model->setId(self::$properties['_id'])
              ->setName(self::$properties['name'])
              ->setIntegrationId(self::$properties['integration_id'])
              ->setGroupId(self::$properties['formgroup_id'])
              ->setRecipientId(self::$properties['formlist_id'])
              ->setActive((bool)self::$properties['isActive'])
              ->setActionUrl(self::$properties['actionUrl'])
              ->setTheme(self::$properties['theme'])
              ->setSchedule(self::$properties['schedule'])
              ->setIntegrationUserId(self::$properties['integration_user_id']);

        return $model;
    }
}
