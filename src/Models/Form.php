<?php

namespace Advision\Fog\Models;

class Form extends Model
{
    protected $name;

    protected $groupId;

    protected $recipientId;

    protected $active;

    protected $theme;

    protected $actionUrl;

    protected $schedule;

    protected function factoryClass()
    {
        return \Advision\Fog\Factories\Form::class;
    }

    public function getEndpoint()
    {
        return 'form';
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * @param string $groupId
     *
     * @return self
     */
    public function setGroupId($groupId)
    {
        $this->groupId = $groupId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRecipientId()
    {
        return $this->recipientId;
    }

    /**
     * @param mixed $recipientId
     *
     * @return self
     */
    public function setRecipientId($recipientId)
    {
        $this->recipientId = $recipientId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param bolean $active
     *
     * @return self
     */
    public function setActive($active)
    {
        $this->active = (bool)$active;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getActionUrl()
    {
        return $this->actionUrl;
    }

    /**
     * @param mixed $actionUrl
     *
     * @return self
     */
    public function setActionUrl($actionUrl)
    {
        $this->actionUrl = $actionUrl;

        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'id'            => $this->getId(),
            'name'          => $this->getName(),
            'schedule'      => $this->getSchedule(),
            'formgroup_id'  => $this->getGroupId(),
            'formlist_id'   => $this->getRecipientId(),
            'isActive'      => $this->getActive(),
            'actionUrl'     => $this->getActionUrl(),
            'theme'         => $this->getTheme()
        ];
    }

    /**
     * @return json
     */
    public function toJson()
    {
        return json_encode($this->toArray());
    }

    /**
     * @return mixed
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * @param mixed $theme
     *
     * @return self
     */
    public function setTheme($theme)
    {
        $this->theme = $theme;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSchedule()
    {
        return $this->schedule;
    }

    /**
     * @param mixed $schedule
     *
     * @return self
     */
    public function setSchedule($schedule)
    {
        $this->schedule = $schedule;

        return $this;
    }
}

