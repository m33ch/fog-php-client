<?php

namespace Advision\Fog\Models;

use Advision\Fog\Contracts\Arrayable;
use Advision\Fog\Contracts\Jsonable;
use Advision\Fog\Exceptions\NoPropertyFound;

abstract class Model implements Arrayable, Jsonable
{
    protected $id = null;

    protected $integrationId = null;

    protected $integrationUserId = null;

    protected $relateds = [];

    public abstract function getEndpoint();

    protected abstract function factoryClass();

    public function getFactory()
    {

        $class = $this->factoryClass();

        if (class_exists($class))
        {
            return $class::getInstance();
        }
    }

    public function with($rel)
    {
        if (in_array($rel, $this->relateds)) return $this;

        $this->relateds[] = $rel;

        return $this;
    }

    public function emptyRelateds()
    {
        return empty($this->relateds);
    }

    public function getRelateds()
    {
        return implode('/',$this->relateds);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function __call($name, $arguments)
    {
        if (method_exists($this, $name) == false)
        {
            throw new NoPropertyFound($name, get_class($this));
        }
    }

    /**
     * @return mixed
     */
    public function getIntegrationId()
    {
        return $this->integrationId;
    }

    /**
     * @param mixed $integrationId
     *
     * @return self
     */
    public function setIntegrationId($integrationId)
    {
        $this->integrationId = $integrationId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIntegrationUserId()
    {
        return $this->integrationUserId;
    }

    /**
     * @param mixed $integrationUserId
     *
     * @return self
     */
    public function setIntegrationUserId($integrationUserId)
    {
        $this->integrationUserId = $integrationUserId;

        return $this;
    }

    /**
     * @param mixed $relateds
     *
     * @return self
     */
    public function setRelateds($relateds)
    {
        $this->relateds = $relateds;

        return $this;
    }
}