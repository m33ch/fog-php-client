<?php

namespace Advision\Fog\Exceptions;

class NoTokenProvided extends \Exception
{
    public function __construct()
    {
        parent::__construct('Token is missing');
    }
}