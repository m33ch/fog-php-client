<?php

namespace Advision\Fog\Exceptions;

class ResponseError extends \Exception
{
    public function __construct($code, $error, $messages)
    {
        $message = 'Response error %s. Messages : %s';

        $this->error = $error;
        $this->err_message = $messages;

        parent::__construct(sprintf($message, $error, $messages), $code);
    }

    public function getErrorCode()
    {
        return $this->error;
    }

    public function getErrorMessage()
    {
        return $this->err_message;
    }
}